"use strict";
let BigNumber = function(val) {
    if (val instanceof BigNumber) {
        this.val = val.val;
        return this;
    }

    if (!val || (val instanceof Array && !val.length)) {
        this.val = [0];
    } else if (val instanceof Array || typeof val === 'string') {
        let firstNonZero = false;

        this.val = [];

        for (let i = 0; i < val.length; i++) {
            let digit = parseInt(val[i]);

            if (isNaN(digit)) {
                continue;
            }

            if (digit !== 0) {
                firstNonZero = true;
            }

            if (firstNonZero) {
                this.val.push(digit);
            }
        }

    } else {
        let positive = val > 0;

        this.val = [];
        val = Math.abs(val);
        while (val > 0) {
            this.val.splice(0, 0, val % 10);
            val = parseInt(val / 10, 10);
        }

        if (!positive) {
            this.val[0] *= -1;
        }
    }

    return this;
}

BigNumber.prototype.valueOf = function(str) {
    let res = this.val.join('');
    return str && res || parseFloat(res, 10);
}

BigNumber.prototype.positivity = function() {
    return this.val[0] / Math.abs(this.val[0]);
}

BigNumber.prototype.numDigits = function() {
    return this.val.length;
}

BigNumber.prototype.sumDigits = function() {
    let temp = new BigNumber(this),
        result = 0;
    while (temp.gt(0)) {
    	let division = temp.div(10);
        result += division.modulo;
        temp = division.quocient;
    }
    return result;
}

BigNumber.prototype.add = function(num) {
    num = new BigNumber(num);

    let thisPositivity = this.positivity(),
        numPositivity = num.positivity(),
        thisAbs = this.abs(),
        numAbs = num.abs();

    if (thisPositivity === 0) {
        return num;
    } else if (numPositivity === 0) {
        return this;
    }

    let result = [],
        normalAdd = thisPositivity === numPositivity,
        num1 = thisAbs,
        num2 = numAbs,
        index1 = num1.numDigits() - 1,
        index2 = num2.numDigits() - 1,
        carry = 0;

    if (!normalAdd && num1.eq(num2)) {
        return new BigNumber();
    }
    
    num1 = (normalAdd || num1.gt(num2)) && thisAbs || numAbs;
    num2 = (normalAdd || num1.gt(num2)) && num.abs() || thisAbs;

    while (Math.max(index1, index2) >= 0) {
        let val1 = num1.val[index1--] || 0,
            val2 = num2.val[index2--] || 0,
            res;

        if (normalAdd) {
            res = val1 + val2 + carry;
            carry = parseInt(res / 10, 10);
            result.splice(0, 0, res % 10);
        } else {
            res = val1 - val2 + carry;
            carry = res < 0 && -1 || 0;
            result.splice(0, 0, res < 0 && 10 - Math.abs(res) || res);
        }
    }

    if (carry) {
        result.splice(0, 0, carry);
    }

    if (normalAdd && thisPositivity < 0  || !normalAdd && (thisPositivity < 0 && thisAbs.gt(numAbs) || numPositivity < 0 && thisAbs.lt(numAbs))) {
        result[0] = -result[0];
    }

    return new BigNumber(result);
}

BigNumber.prototype.sub = function(num) {
    let subtractor = (new BigNumber(num));

    subtractor.val[0] = -subtractor.val[0];

    return this.add(subtractor);
}

BigNumber.prototype.inc = function() {
    this.val = this.add(1).val;
}

BigNumber.prototype.dec = function() {
    this.val = this.add(-1).val;
}

BigNumber.prototype.mult = function(num) {
    num = new BigNumber(num);

    if (this.eq(0) || num.eq(0)) {
        return new BigNumber();
    }

    let result = [],
        carry = 0,
        positive = this.positivity() * num.positivity() >= 0,
        numerator = this.abs(),
        multiplier = num.abs(),
        indexMultiplier = num.numDigits() - 1;

    if (this.eq(0) || num.eq(0)) {
        return new BigNumber();
    }

    while (indexMultiplier >= 0) {
        let indexNumerator = numerator.numDigits() - 1,
            counter = 0;
        while (indexNumerator >= 0) {
            let res = (result[indexMultiplier + indexNumerator] || 0) + numerator.val[indexNumerator] * multiplier.val[indexMultiplier] + carry;
            result[indexMultiplier + indexNumerator] = res % 10;
            carry = parseInt(res / 10, 10);
            indexNumerator--;
        }


        while (carry) {
            let carryIndex = indexMultiplier + indexNumerator,
                res;
            if (carryIndex >= 0) {
                res = (result[carryIndex] || 0) + carry;
                result[carryIndex] = res % 10;
            } else {
                res = carry % 10
                result.splice(0, 0, res);
                counter++;
            }
            carry = parseInt(res / 10, 10);
        }

        indexMultiplier--;
    }

    while (carry) {
            let carryIndex = indexMultiplier + indexNumerator,
                res;
            if (carryIndex >= 0) {
                res = (result[carryIndex] || 0) + carry;
                result[carryIndex] = res % 10;
            } else {
                res = carry % 10
                result.splice(0, 0, res);
            }
            carry = parseInt(res / 10, 10);
        }

    if (!positive) {
        result[0] = -result[0];
    }

    return new BigNumber(result);
}

BigNumber.prototype._smallDivision = function(num) {
    let modulo = new BigNumber(this),
        quocient = new BigNumber();

    while (modulo.gte(num)) {
        modulo = modulo.sub(num);
        quocient.inc();
    }

    return {
    	quocient: quocient,
    	modulo: modulo
    };
}

BigNumber.prototype.div = function(num) {
    num = new BigNumber(num);

    if (num.eq(0)) {
        return new BigNumber;
    }

    let index = 0,
        thisLength = this.numDigits(),
        modulo = new BigNumber(),
        quocient = new BigNumber(),
        division;

    while (index < thisLength) {
        while (modulo.lt(num) && index < thisLength) {
            modulo = modulo.mult(10).add(this.val[index++]);
            if (modulo.eq(0)) {
                quocient = quocient.mult(10);
            }
        }

        if (index === thisLength && modulo.valueOf() === 0) {
            break;
        }

        division = modulo._smallDivision(num);
        quocient = quocient.mult(10).add(division.quocient);
        modulo = division.modulo;
    }
    
    return {
    	quocient: quocient,
    	modulo: modulo
    };
}

BigNumber.prototype.mod = function(num) {
    return this.div(num).modulo;
}

BigNumber.prototype.pow = function(power) {
    if (power < 0) {
        return new BigNumber();
    }

    if (power === 0) {
        return new BigNumber(1);
    }

    let result = new BigNumber(this);
    while (power-- > 1) {
        result = result.mult(this);
    }
    return result;
}

BigNumber.prototype.fact = function() {
    let result = new BigNumber(1),
        counter = new BigNumber(this);
    
    if (this.eq(0)) {
        return result;
    }

    while (counter.gt(1)) {
        result = result.mult(counter);
        counter.dec();
    }

    return result;
}


BigNumber.prototype.gt = function(num) {
    num = new BigNumber(num);
    let thisLength = this.numDigits(),
        numLength = num.numDigits(),
        thisPositivity = this.positivity(),
        numPositivity = num.positivity();
    
    if (thisPositivity !== numPositivity) {
        return thisPositivity > numPositivity;
    }
    
    if (thisLength !== numLength) {
        return thisLength > numLength;
    }

    for (let i = 0; i < thisLength; i++) {
        let val1 = this.val[i],
            val2 = num.val[i];

        if (val1 != val2) {
            return val1 > val2;
        }
    }

    return false;
}

BigNumber.prototype.lt = function(num) {
    num = new BigNumber(num);
    let thisLength = this.numDigits(),
        numLength = num.numDigits(),
        thisPositivity = this.positivity(),
        numPositivity = num.positivity();
    
    if (thisPositivity !== numPositivity) {
        return thisPositivity < numPositivity;
    }

    if (thisLength !== numLength) {
        return thisLength < numLength;
    }

    for (let i = 0; i < thisLength; i++) {
        let val1 = this.val[i],
            val2 = num.val[i];

        if (val1 != val2) {
            return val1 < val2;
        }
    }

    return false;
}

BigNumber.prototype.eq = function(num) {
    num = new BigNumber(num);
    let thisLength = this.numDigits(),
        numLength = num.numDigits();

    if (thisLength !== numLength) {
        return false;
    }

    for (let i = 0; i < thisLength; i++) {
        if (this.val[i] !== num.val[i]) {
            return false;
        }
    }

    return true;
}

BigNumber.prototype.neq = function(num) {
    return !this.eq(num);
}

BigNumber.prototype.gte = function(num) {
    return this.gt(num) || this.eq(num);
}

BigNumber.prototype.lte = function(num) {
    return this.lt(num) || this.eq(num);
}

BigNumber.prototype.abs = function() {
    let result = new BigNumber(this);
    result.val[0] = Math.abs(result.val[0]);
    return result;
}